package com.csys.appel.web.rest;

import com.csys.appel.dto.FournisseurDTO;
import com.csys.appel.service.FournisseurService;

import java.lang.Integer;
import java.lang.String;
import java.lang.Void;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import javax.validation.Valid;

import com.csys.appel.util.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Fournisseur.
 */
@RestController
@RequestMapping("/api")
public class FournisseurResource {
    private static final String ENTITY_NAME = "fournisseur";

    private final FournisseurService fournisseurService;

    private final Logger log = LoggerFactory.getLogger(FournisseurService.class);

    public FournisseurResource(FournisseurService fournisseurService) {
        this.fournisseurService = fournisseurService;
    }

    /**
     * POST  /fournisseurs : Create a new fournisseur.
     *
     * @param fournisseurDTO
     * @param bindingResult
     * @return the ResponseEntity with status 201 (Created) and with body the new fournisseur, or with status 400 (Bad Request) if the fournisseur has already an ID
     * @throws URISyntaxException                                           if the Location URI syntax is incorrect
     * @throws org.springframework.web.bind.MethodArgumentNotValidException
     */
    @PostMapping("/fournisseurs")
    public ResponseEntity<FournisseurDTO> createFournisseur(@Valid @RequestBody FournisseurDTO fournisseurDTO, BindingResult bindingResult) throws URISyntaxException, MethodArgumentNotValidException {
        log.debug("REST request to save Fournisseur : {}", fournisseurDTO);
        if (fournisseurDTO.getId() != null) {
            bindingResult.addError(new FieldError("FournisseurDTO", "id", "POST method does not accepte " + ENTITY_NAME + " with code"));
            throw new MethodArgumentNotValidException(null, bindingResult);
        }
        if (bindingResult.hasErrors()) {
            throw new MethodArgumentNotValidException(null, bindingResult);
        }
        FournisseurDTO result = fournisseurService.save(fournisseurDTO);
        return ResponseEntity.created(new URI("/api/fournisseurs/" + result.getId())).body(result);
    }

    /**
     * PUT  /fournisseurs : Updates an existing fournisseur.
     *
     * @param id
     * @param fournisseurDTO the fournisseur to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated fournisseur,
     * or with status 400 (Bad Request) if the fournisseur is not valid,
     * or with status 500 (Internal Server Error) if the fournisseur couldn't be updated
     * @throws org.springframework.web.bind.MethodArgumentNotValidException
     */
    @PutMapping("/fournisseurs/{id}")
    public ResponseEntity<FournisseurDTO> updateFournisseur(@PathVariable Integer id, @Valid @RequestBody FournisseurDTO fournisseurDTO) throws MethodArgumentNotValidException {
        log.debug("Request to update Fournisseur: {}", id);
        fournisseurDTO.setId(id);
        FournisseurDTO result = fournisseurService.update(fournisseurDTO);
        return ResponseEntity.ok().body(result);
    }

    /**
     * GET /fournisseurs/{id} : get the "id" fournisseur.
     *
     * @param id the id of the fournisseur to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body of fournisseur, or with status 404 (Not Found)
     */
    @GetMapping("/fournisseurs/{id}")
    public ResponseEntity<FournisseurDTO> getFournisseur(@PathVariable Integer id) {
        log.debug("Request to get Fournisseur: {}", id);
        FournisseurDTO dto = fournisseurService.findById(id);
        RestPreconditions.checkFound(dto, "fournisseur.NotFound");
        return ResponseEntity.ok().body(dto);
    }

    /**
     * GET /fournisseurs : get all the fournisseurs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of fournisseurs in body
     */
    @GetMapping("/fournisseurs")
    public Collection<FournisseurDTO> getAllFournisseurs() {
        log.debug("Request to get all  Fournisseurs : {}");
        return fournisseurService.findAll();
    }

    /**
     * DELETE  /fournisseurs/{id} : delete the "id" fournisseur.
     *
     * @param id the id of the fournisseur to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/fournisseurs/{id}")
    public ResponseEntity<Void> deleteFournisseur(@PathVariable Integer id) {
        log.debug("Request to delete Fournisseur: {}", id);
        fournisseurService.delete(id);
        return ResponseEntity.ok().build();
    }
}

