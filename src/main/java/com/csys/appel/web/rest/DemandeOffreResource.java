package com.csys.appel.web.rest;

import com.csys.appel.dto.DemandeOffreDTO;
import com.csys.appel.service.DemandeOffreService;
import java.lang.Integer;
import java.lang.String;
import java.lang.Void;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import javax.validation.Valid;

import com.csys.appel.util.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing DemandeOffre.
 */
@RestController
@RequestMapping("/api")
public class DemandeOffreResource {
  private static final String ENTITY_NAME = "demandeoffre";

  private final DemandeOffreService demandeoffreService;

  private final Logger log = LoggerFactory.getLogger(DemandeOffreService.class);

  public DemandeOffreResource(DemandeOffreService demandeoffreService) {
    this.demandeoffreService=demandeoffreService;
  }

  /**
   * POST  /demandeoffres : Create a new demandeoffre.
   *
   * @param demandeoffreDTO
   * @param bindingResult
   * @return the ResponseEntity with status 201 (Created) and with body the new demandeoffre, or with status 400 (Bad Request) if the demandeoffre has already an ID
   * @throws URISyntaxException if the Location URI syntax is incorrect
   * @throws org.springframework.web.bind.MethodArgumentNotValidException
   */
  @PostMapping("/demandeoffres")
  public ResponseEntity<DemandeOffreDTO> createDemandeOffre(@Valid @RequestBody DemandeOffreDTO demandeoffreDTO, BindingResult bindingResult) throws URISyntaxException, MethodArgumentNotValidException {
    log.debug("REST request to save DemandeOffre : {}", demandeoffreDTO);
    if ( demandeoffreDTO.getId() != null) {
      bindingResult.addError( new FieldError("DemandeOffreDTO","id","POST method does not accepte "+ENTITY_NAME+" with code"));
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    if (bindingResult.hasErrors()) {
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    DemandeOffreDTO result = demandeoffreService.save(demandeoffreDTO);
    return ResponseEntity.created( new URI("/api/demandeoffres/"+ result.getId())).body(result);
  }

  /**
   * PUT  /demandeoffres : Updates an existing demandeoffre.
   *
   * @param id
   * @param demandeoffreDTO the demandeoffre to update
   * @return the ResponseEntity with status 200 (OK) and with body the updated demandeoffre,
   * or with status 400 (Bad Request) if the demandeoffre is not valid,
   * or with status 500 (Internal Server Error) if the demandeoffre couldn't be updated
   * @throws org.springframework.web.bind.MethodArgumentNotValidException
   */
  @PutMapping("/demandeoffres/{id}")
  public ResponseEntity<DemandeOffreDTO> updateDemandeOffre(@PathVariable Integer id, @Valid @RequestBody DemandeOffreDTO demandeoffreDTO) throws MethodArgumentNotValidException {
    log.debug("Request to update DemandeOffre: {}",id);
    demandeoffreDTO.setId(id);
    DemandeOffreDTO result =demandeoffreService.update(demandeoffreDTO);
    return ResponseEntity.ok().body(result);
  }

  /**
   * GET /demandeoffres/{id} : get the "id" demandeoffre.
   *
   * @param id the id of the demandeoffre to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body of demandeoffre, or with status 404 (Not Found)
   */
  @GetMapping("/demandeoffres/{id}")
  public ResponseEntity<DemandeOffreDTO> getDemandeOffre(@PathVariable Integer id) {
    log.debug("Request to get DemandeOffre: {}",id);
    DemandeOffreDTO dto = demandeoffreService.findOne(id);
    RestPreconditions.checkFound(dto, "demandeoffre.NotFound");
    return ResponseEntity.ok().body(dto);
  }

  /**
   * GET /demandeoffres : get all the demandeoffres.
   *
   * @return the ResponseEntity with status 200 (OK) and the list of demandeoffres in body
   */
  @GetMapping("/demandeoffres")
  public Collection<DemandeOffreDTO> getAllDemandeOffres() {
    log.debug("Request to get all  DemandeOffres : {}");
    return demandeoffreService.findAll();
  }

  /**
   * DELETE  /demandeoffres/{id} : delete the "id" demandeoffre.
   *
   * @param id the id of the demandeoffre to delete
   * @return the ResponseEntity with status 200 (OK)
   */
  @DeleteMapping("/demandeoffres/{id}")
  public ResponseEntity<Void> deleteDemandeOffre(@PathVariable Integer id) {
    log.debug("Request to delete DemandeOffre: {}",id);
    demandeoffreService.delete(id);
    return ResponseEntity.ok().build();
  }
}

