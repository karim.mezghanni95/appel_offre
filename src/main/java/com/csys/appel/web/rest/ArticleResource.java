package com.csys.appel.web.rest;

import com.csys.appel.dto.ArticleDTO;
import com.csys.appel.service.ArticleService;
import java.lang.Integer;
import java.lang.String;
import java.lang.Void;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import javax.validation.Valid;

import com.csys.appel.util.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Article.
 */
@RestController
@RequestMapping("/api")
public class ArticleResource {
  private static final String ENTITY_NAME = "article";

  private final ArticleService articleService;

  private final Logger log = LoggerFactory.getLogger(ArticleService.class);

  public ArticleResource(ArticleService articleService) {
    this.articleService=articleService;
  }

  /**
   * POST  /articles : Create a new article.
   *
   * @param articleDTO
   * @param bindingResult
   * @return the ResponseEntity with status 201 (Created) and with body the new article, or with status 400 (Bad Request) if the article has already an ID
   * @throws URISyntaxException if the Location URI syntax is incorrect
   * @throws org.springframework.web.bind.MethodArgumentNotValidException
   */
  @PostMapping("/articles")
  public ResponseEntity<ArticleDTO> createArticle(@Valid @RequestBody ArticleDTO articleDTO, BindingResult bindingResult) throws URISyntaxException, MethodArgumentNotValidException {
    log.debug("REST request to save Article : {}", articleDTO);
    if ( articleDTO.getId() != null) {
      bindingResult.addError( new FieldError("ArticleDTO","id","POST method does not accepte "+ENTITY_NAME+" with code"));
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    if (bindingResult.hasErrors()) {
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    ArticleDTO result = articleService.save(articleDTO);
    return ResponseEntity.created( new URI("/api/articles/"+ result.getId())).body(result);
  }

  /**
   * PUT  /articles : Updates an existing article.
   *
   * @param id
   * @param articleDTO the article to update
   * @return the ResponseEntity with status 200 (OK) and with body the updated article,
   * or with status 400 (Bad Request) if the article is not valid,
   * or with status 500 (Internal Server Error) if the article couldn't be updated
   * @throws org.springframework.web.bind.MethodArgumentNotValidException
   */
  @PutMapping("/articles/{id}")
  public ResponseEntity<ArticleDTO> updateArticle(@PathVariable Integer id, @Valid @RequestBody ArticleDTO articleDTO) throws MethodArgumentNotValidException {
    log.debug("Request to update Article: {}",id);
    articleDTO.setId(id);
    ArticleDTO result =articleService.update(articleDTO);
    return ResponseEntity.ok().body(result);
  }

  /**
   * GET /articles/{id} : get the "id" article.
   *
   * @param id the id of the article to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body of article, or with status 404 (Not Found)
   */
  @GetMapping("/articles/{id}")
  public ResponseEntity<ArticleDTO> getArticle(@PathVariable Integer id) {
    log.debug("Request to get Article: {}",id);
    ArticleDTO dto = articleService.findOne(id);
    RestPreconditions.checkFound(dto, "article.NotFound");
    return ResponseEntity.ok().body(dto);
  }

  /**
   * GET /articles : get all the articles.
   *
   * @return the ResponseEntity with status 200 (OK) and the list of articles in body
   */
  @GetMapping("/articles")
  public Collection<ArticleDTO> getAllArticles() {
    log.debug("Request to get all  Articles : {}");
    return articleService.findAll();
  }

  /**
   * DELETE  /articles/{id} : delete the "id" article.
   *
   * @param id the id of the article to delete
   * @return the ResponseEntity with status 200 (OK)
   */
  @DeleteMapping("/articles/{id}")
  public ResponseEntity<Void> deleteArticle(@PathVariable Integer id) {
    log.debug("Request to delete Article: {}",id);
    articleService.delete(id);
    return ResponseEntity.ok().build();
  }
}

