package com.csys.appel.web.rest;

import com.csys.appel.dto.OffreDTO;
import com.csys.appel.service.OffreService;

import java.lang.Integer;
import java.lang.String;
import java.lang.Void;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import javax.validation.Valid;

import com.csys.appel.util.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Offre.
 */
@RestController
@RequestMapping("/api")
public class OffreResource {
    private static final String ENTITY_NAME = "offre";

    private final OffreService offreService;

    private final Logger log = LoggerFactory.getLogger(OffreService.class);

    public OffreResource(OffreService offreService) {
        this.offreService = offreService;
    }

    /**
     * POST  /offres : Create a new offre.
     *
     * @param offreDTO
     * @param bindingResult
     * @return the ResponseEntity with status 201 (Created) and with body the new offre, or with status 400 (Bad Request) if the offre has already an ID
     * @throws URISyntaxException                                           if the Location URI syntax is incorrect
     * @throws org.springframework.web.bind.MethodArgumentNotValidException
     */
    @PostMapping("/offres")
    public ResponseEntity<OffreDTO> createOffre(@Valid @RequestBody OffreDTO offreDTO, BindingResult bindingResult) throws URISyntaxException, MethodArgumentNotValidException {
        log.debug("REST request to save Offre : {}", offreDTO);
        if (offreDTO.getId() != null) {
            bindingResult.addError(new FieldError("OffreDTO", "id", "POST method does not accepte " + ENTITY_NAME + " with code"));
            throw new MethodArgumentNotValidException(null, bindingResult);
        }
        if (bindingResult.hasErrors()) {
            throw new MethodArgumentNotValidException(null, bindingResult);
        }
        OffreDTO result = offreService.save(offreDTO);
        return ResponseEntity.created(new URI("/api/offres/" + result.getId())).body(result);
    }

    /**
     * PUT  /offres : Updates an existing offre.
     *
     * @param id
     * @param offreDTO the offre to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated offre,
     * or with status 400 (Bad Request) if the offre is not valid,
     * or with status 500 (Internal Server Error) if the offre couldn't be updated
     * @throws org.springframework.web.bind.MethodArgumentNotValidException
     */
    @PutMapping("/offres/{id}")
    public ResponseEntity<OffreDTO> updateOffre(@PathVariable Integer id, @Valid @RequestBody OffreDTO offreDTO) throws MethodArgumentNotValidException {
        log.debug("Request to update Offre: {}", id);
        offreDTO.setId(id);
        OffreDTO result = offreService.update(offreDTO);
        return ResponseEntity.ok().body(result);
    }

    /**
     * GET /offres/{id} : get the "id" offre.
     *
     * @param id the id of the offre to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body of offre, or with status 404 (Not Found)
     */
    @GetMapping("/offres/{id}")
    public ResponseEntity<OffreDTO> getOffre(@PathVariable Integer id) {
        log.debug("Request to get Offre: {}", id);
        OffreDTO dto = offreService.findById(id);
        RestPreconditions.checkFound(dto, "offre.NotFound");
        return ResponseEntity.ok().body(dto);
    }

    /**
     * GET /offres : get all the offres.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of offres in body
     */
    @GetMapping("/offres")
    public Collection<OffreDTO> getAllOffres() {
        log.debug("Request to get all  Offres : {}");
        return offreService.findAll();
    }

    /**
     * DELETE  /offres/{id} : delete the "id" offre.
     *
     * @param id the id of the offre to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/offres/{id}")
    public ResponseEntity<Void> deleteOffre(@PathVariable Integer id) {
        log.debug("Request to delete Offre: {}", id);
        offreService.delete(id);
        return ResponseEntity.ok().build();
    }
}

