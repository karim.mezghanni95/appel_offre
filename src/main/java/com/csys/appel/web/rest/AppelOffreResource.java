package com.csys.appel.web.rest;

import com.csys.appel.dto.AppelOffreDTO;
import com.csys.appel.service.AppelOffreService;
import java.lang.Integer;
import java.lang.String;
import java.lang.Void;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import javax.validation.Valid;

import com.csys.appel.util.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing AppelOffre.
 */
@RestController
@RequestMapping("/api")
public class AppelOffreResource {
  private static final String ENTITY_NAME = "appeloffre";

  private final AppelOffreService appeloffreService;

  private final Logger log = LoggerFactory.getLogger(AppelOffreService.class);

  public AppelOffreResource(AppelOffreService appeloffreService) {
    this.appeloffreService=appeloffreService;
  }

  /**
   * POST  /appeloffres : Create a new appeloffre.
   *
   * @param appeloffreDTO
   * @param bindingResult
   * @return the ResponseEntity with status 201 (Created) and with body the new appeloffre, or with status 400 (Bad Request) if the appeloffre has already an ID
   * @throws URISyntaxException if the Location URI syntax is incorrect
   * @throws org.springframework.web.bind.MethodArgumentNotValidException
   */
  @PostMapping("/appeloffres")
  public ResponseEntity<AppelOffreDTO> createAppelOffre(@Valid @RequestBody AppelOffreDTO appeloffreDTO, BindingResult bindingResult) throws URISyntaxException, MethodArgumentNotValidException {
    log.debug("REST request to save AppelOffre : {}", appeloffreDTO);
    if ( appeloffreDTO.getId() != null) {
      bindingResult.addError( new FieldError("AppelOffreDTO","id","POST method does not accepte "+ENTITY_NAME+" with code"));
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    if (bindingResult.hasErrors()) {
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    AppelOffreDTO result = appeloffreService.save(appeloffreDTO);
    return ResponseEntity.created( new URI("/api/appeloffres/"+ result.getId())).body(result);
  }

  /**
   * PUT  /appeloffres : Updates an existing appeloffre.
   *
   * @param id
   * @param appeloffreDTO the appeloffre to update
   * @return the ResponseEntity with status 200 (OK) and with body the updated appeloffre,
   * or with status 400 (Bad Request) if the appeloffre is not valid,
   * or with status 500 (Internal Server Error) if the appeloffre couldn't be updated
   * @throws org.springframework.web.bind.MethodArgumentNotValidException
   */
  @PutMapping("/appeloffres/{id}")
  public ResponseEntity<AppelOffreDTO> updateAppelOffre(@PathVariable Integer id, @Valid @RequestBody AppelOffreDTO appeloffreDTO) throws MethodArgumentNotValidException {
    log.debug("Request to update AppelOffre: {}",id);
    appeloffreDTO.setId(id);
    AppelOffreDTO result =appeloffreService.update(appeloffreDTO);
    return ResponseEntity.ok().body(result);
  }

  /**
   * GET /appeloffres/{id} : get the "id" appeloffre.
   *
   * @param id the id of the appeloffre to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body of appeloffre, or with status 404 (Not Found)
   */
  @GetMapping("/appeloffres/{id}")
  public ResponseEntity<AppelOffreDTO> getAppelOffre(@PathVariable Integer id) {
    log.debug("Request to get AppelOffre: {}",id);
    AppelOffreDTO dto = appeloffreService.findById(id);
    RestPreconditions.checkFound(dto, "appeloffre.NotFound");
    return ResponseEntity.ok().body(dto);
  }

  /**
   * GET /appeloffres : get all the appeloffres.
   *
   * @return the ResponseEntity with status 200 (OK) and the list of appeloffres in body
   */
  @GetMapping("/appeloffres")
  public Collection<AppelOffreDTO> getAllAppelOffres() {
    log.debug("Request to get all  AppelOffres : {}");
    return appeloffreService.findAll();
  }

  /**
   * DELETE  /appeloffres/{id} : delete the "id" appeloffre.
   *
   * @param id the id of the appeloffre to delete
   * @return the ResponseEntity with status 200 (OK)
   */
  @DeleteMapping("/appeloffres/{id}")
  public ResponseEntity<Void> deleteAppelOffre(@PathVariable Integer id) {
    log.debug("Request to delete AppelOffre: {}",id);
    appeloffreService.delete(id);
    return ResponseEntity.ok().build();
  }
}

