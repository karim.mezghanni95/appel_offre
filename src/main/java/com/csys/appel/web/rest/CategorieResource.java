package com.csys.appel.web.rest;

import com.csys.appel.dto.CategorieDTO;
import com.csys.appel.service.CategorieService;
import java.lang.Integer;
import java.lang.String;
import java.lang.Void;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import javax.validation.Valid;

import com.csys.appel.util.RestPreconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Categorie.
 */
@RestController
@RequestMapping("/api")
public class CategorieResource {
  private static final String ENTITY_NAME = "categorie";

  private final CategorieService categorieService;

  private final Logger log = LoggerFactory.getLogger(CategorieService.class);

  public CategorieResource(CategorieService categorieService) {
    this.categorieService=categorieService;
  }

  /**
   * POST  /categories : Create a new categorie.
   *
   * @param categorieDTO
   * @param bindingResult
   * @return the ResponseEntity with status 201 (Created) and with body the new categorie, or with status 400 (Bad Request) if the categorie has already an ID
   * @throws URISyntaxException if the Location URI syntax is incorrect
   * @throws org.springframework.web.bind.MethodArgumentNotValidException
   */
  @PostMapping("/categories")
  public ResponseEntity<CategorieDTO> createCategorie(@Valid @RequestBody CategorieDTO categorieDTO, BindingResult bindingResult) throws URISyntaxException, MethodArgumentNotValidException {
    log.debug("REST request to save Categorie : {}", categorieDTO);
    if ( categorieDTO.getId() != null) {
      bindingResult.addError( new FieldError("CategorieDTO","id","POST method does not accepte "+ENTITY_NAME+" with code"));
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    if (bindingResult.hasErrors()) {
      throw new MethodArgumentNotValidException(null, bindingResult);
    }
    CategorieDTO result = categorieService.save(categorieDTO);
    return ResponseEntity.created( new URI("/api/categories/"+ result.getId())).body(result);
  }

  /**
   * PUT  /categories : Updates an existing categorie.
   *
   * @param id
   * @param categorieDTO the categorie to update
   * @return the ResponseEntity with status 200 (OK) and with body the updated categorie,
   * or with status 400 (Bad Request) if the categorie is not valid,
   * or with status 500 (Internal Server Error) if the categorie couldn't be updated
   * @throws org.springframework.web.bind.MethodArgumentNotValidException
   */
  @PutMapping("/categories/{id}")
  public ResponseEntity<CategorieDTO> updateCategorie(@PathVariable Integer id, @Valid @RequestBody CategorieDTO categorieDTO) throws MethodArgumentNotValidException {
    log.debug("Request to update Categorie: {}",id);
    categorieDTO.setId(id);
    CategorieDTO result =categorieService.update(categorieDTO);
    return ResponseEntity.ok().body(result);
  }

  /**
   * GET /categories/{id} : get the "id" categorie.
   *
   * @param id the id of the categorie to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body of categorie, or with status 404 (Not Found)
   */
  @GetMapping("/categories/{id}")
  public ResponseEntity<CategorieDTO> getCategorie(@PathVariable Integer id) {
    log.debug("Request to get Categorie: {}",id);
    CategorieDTO dto = categorieService.findById(id);
    RestPreconditions.checkFound(dto, "categorie.NotFound");
    return ResponseEntity.ok().body(dto);
  }

  /**
   * GET /categories : get all the categories.
   *
   * @return the ResponseEntity with status 200 (OK) and the list of categories in body
   */
  @GetMapping("/categories")
  public Collection<CategorieDTO> getAllCategories() {
    log.debug("Request to get all  Categories : {}");
    return categorieService.findAll();
  }

  /**
   * DELETE  /categories/{id} : delete the "id" categorie.
   *
   * @param id the id of the categorie to delete
   * @return the ResponseEntity with status 200 (OK)
   */
  @DeleteMapping("/categories/{id}")
  public ResponseEntity<Void> deleteCategorie(@PathVariable Integer id) {
    log.debug("Request to delete Categorie: {}",id);
    categorieService.delete(id);
    return ResponseEntity.ok().build();
  }
}

