package com.csys.appel.factory;

import com.csys.appel.domain.AppelOffre;
import com.csys.appel.domain.DemandeOffre;
import com.csys.appel.dto.AppelOffreDTO;
import com.csys.appel.dto.DemandeOffreDTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AppelOffreFactory {
    public static AppelOffreDTO appeloffreToAppelOffreDTO(AppelOffre appeloffre) {
        AppelOffreDTO appeloffreDTO = new AppelOffreDTO();
        appeloffreDTO.setId(appeloffre.getId());
        appeloffreDTO.setNumero(appeloffre.getNumero());
        appeloffreDTO.setDateDebut(appeloffre.getDateDebut());
        appeloffreDTO.setDateFin(appeloffre.getDateFin());
        appeloffreDTO.setExercice(appeloffre.getExercice());
        appeloffreDTO.setDesignation(appeloffre.getDesignation());
        Collection<DemandeOffreDTO> demandeOffreCollectionDtos = new ArrayList<>();
        appeloffre.getDemandeOffreCollection().forEach(x -> {
            DemandeOffreDTO demandeoffreDto = new DemandeOffreDTO();
            demandeoffreDto = DemandeOffreFactory.demandeoffreToDemandeOffreDTO(x);
            demandeOffreCollectionDtos.add(demandeoffreDto);
        });
        if (appeloffreDTO.getDemandeOffreCollection() != null) {
            appeloffreDTO.getDemandeOffreCollection().clear();
            appeloffreDTO.getDemandeOffreCollection().addAll(demandeOffreCollectionDtos);
        } else {
            appeloffreDTO.setDemandeOffreCollection(demandeOffreCollectionDtos);
        }
        return appeloffreDTO;
    }

    public static AppelOffre appeloffreDTOToAppelOffre(AppelOffreDTO appeloffreDTO) {
        AppelOffre appeloffre = new AppelOffre();
        appeloffre.setId(appeloffreDTO.getId());
        appeloffre.setNumero(appeloffreDTO.getNumero());
        appeloffre.setDateDebut(appeloffreDTO.getDateDebut());
        appeloffre.setDateFin(appeloffreDTO.getDateFin());
        appeloffre.setExercice(appeloffreDTO.getExercice());
        appeloffre.setDesignation(appeloffreDTO.getDesignation());
        Collection<DemandeOffre> demandeOffreCollections = new ArrayList<>();
        appeloffreDTO.getDemandeOffreCollection().forEach(x -> {
            DemandeOffre demandeoffre = new DemandeOffre();
            demandeoffre = DemandeOffreFactory.demandeoffreDTOToDemandeOffre(x);
            demandeOffreCollections.add(demandeoffre);
        });
        if (appeloffre.getDemandeOffreCollection() != null) {
            appeloffre.getDemandeOffreCollection().clear();
            appeloffre.getDemandeOffreCollection().addAll(demandeOffreCollections);
        } else {
            appeloffre.setDemandeOffreCollection(demandeOffreCollections);
        }
        return appeloffre;
    }

    public static Collection<AppelOffreDTO> appeloffreToAppelOffreDTOs(Collection<AppelOffre> appeloffres) {
        List<AppelOffreDTO> appeloffresDTO = new ArrayList<>();
        appeloffres.forEach(x -> {
            appeloffresDTO.add(appeloffreToAppelOffreDTO(x));
        });
        return appeloffresDTO;
    }

    public static AppelOffreDTO lazyappeloffreToAppelOffreDTO(AppelOffre appeloffre) {
        AppelOffreDTO appeloffreDTO = new AppelOffreDTO();
        appeloffreDTO.setId(appeloffre.getId());
        appeloffreDTO.setNumero(appeloffre.getNumero());
        appeloffreDTO.setDateDebut(appeloffre.getDateDebut());
        appeloffreDTO.setDateFin(appeloffre.getDateFin());
        appeloffreDTO.setExercice(appeloffre.getExercice());
        appeloffreDTO.setDesignation(appeloffre.getDesignation());
        return appeloffreDTO;
    }

    public static Collection<AppelOffreDTO> lazyappeloffreToAppelOffreDTOs(Collection<AppelOffre> appeloffres) {
        List<AppelOffreDTO> appeloffresDTO = new ArrayList<>();
        appeloffres.forEach(x -> {
            appeloffresDTO.add(lazyappeloffreToAppelOffreDTO(x));
        });
        return appeloffresDTO;
    }
}

