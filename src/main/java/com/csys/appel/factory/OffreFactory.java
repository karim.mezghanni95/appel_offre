package com.csys.appel.factory;

import com.csys.appel.domain.Offre;
import com.csys.appel.dto.OffreDTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class OffreFactory {
    public static OffreDTO offreToOffreDTO(Offre offre) {
        OffreDTO offreDTO = new OffreDTO();
        offreDTO.setId(offre.getId());
        offreDTO.setUniteMesure(offre.getUniteMesure());
        offreDTO.setMarque(offre.getMarque());
        offreDTO.setPrixUnitaire(offre.getPrixUnitaire());
        offreDTO.setOrigine(offre.getOrigine());
        offreDTO.setDelaiLivraison(offre.getDelaiLivraison());
        offreDTO.setObservation(offre.getObservation());
        offreDTO.setAmc(offre.getAmc());
        offreDTO.setEchantillon(offre.getEchantillon());
        offreDTO.setFodec(offre.getFodec());
        offreDTO.setAvecCodeBarre(offre.getAvecCodeBarre());
        offreDTO.setConditionnement(offre.getConditionnement());
        offreDTO.setPrixConditionnement(offre.getPrixConditionnement());
        offreDTO.setDemandeOffre(offre.getDemandeOffre());
        offreDTO.setFournisseur(offre.getFournisseur());
        offreDTO.setTva(offre.getTva());
        return offreDTO;
    }

    public static Offre offreDTOToOffre(OffreDTO offreDTO) {
        Offre offre = new Offre();
        offre.setId(offreDTO.getId());
        offre.setUniteMesure(offreDTO.getUniteMesure());
        offre.setMarque(offreDTO.getMarque());
        offre.setPrixUnitaire(offreDTO.getPrixUnitaire());
        offre.setOrigine(offreDTO.getOrigine());
        offre.setDelaiLivraison(offreDTO.getDelaiLivraison());
        offre.setObservation(offreDTO.getObservation());
        offre.setAmc(offreDTO.getAmc());
        offre.setEchantillon(offreDTO.getEchantillon());
        offre.setFodec(offreDTO.getFodec());
        offre.setAvecCodeBarre(offreDTO.getAvecCodeBarre());
        offre.setConditionnement(offreDTO.getConditionnement());
        offre.setPrixConditionnement(offreDTO.getPrixConditionnement());
        offre.setDemandeOffre(offreDTO.getDemandeOffre());
        offre.setFournisseur(offreDTO.getFournisseur());
        offre.setTva(offreDTO.getTva());
        return offre;
    }

    public static Collection<OffreDTO> offreToOffreDTOs(Collection<Offre> offres) {
        List<OffreDTO> offresDTO = new ArrayList<>();
        offres.forEach(x -> {
            offresDTO.add(offreToOffreDTO(x));
        });
        return offresDTO;
    }
}

