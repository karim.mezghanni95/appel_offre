package com.csys.appel.factory;

import com.csys.appel.domain.Fournisseur;
import com.csys.appel.domain.Offre;
import com.csys.appel.dto.FournisseurDTO;
import com.csys.appel.dto.OffreDTO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FournisseurFactory {
  public static FournisseurDTO fournisseurToFournisseurDTO(Fournisseur fournisseur) {
    FournisseurDTO fournisseurDTO=new FournisseurDTO();
    fournisseurDTO.setId(fournisseur.getId());
    fournisseurDTO.setRaisonSociale(fournisseur.getRaisonSociale());
    fournisseurDTO.setMatriculeFiscale(fournisseur.getMatriculeFiscale());
    fournisseurDTO.setAdresse(fournisseur.getAdresse());
    fournisseurDTO.setUtilisateur(fournisseur.getUtilisateur());
    Collection<OffreDTO> offreCollectionDtos = new ArrayList<>();
    fournisseur.getOffreCollection().forEach(x -> {
      OffreDTO offreDto = new OffreDTO();
      offreDto = OffreFactory.offreToOffreDTO(x);
      offreCollectionDtos.add(offreDto);
    } );
    if(fournisseurDTO.getOffreCollection() !=null) {
      fournisseurDTO.getOffreCollection().clear();
      fournisseurDTO.getOffreCollection().addAll(offreCollectionDtos);
    }
    else {
      fournisseurDTO.setOffreCollection(offreCollectionDtos);
    }
    return fournisseurDTO;
  }

  public static Fournisseur fournisseurDTOToFournisseur(FournisseurDTO fournisseurDTO) {
    Fournisseur fournisseur=new Fournisseur();
    fournisseur.setId(fournisseurDTO.getId());
    fournisseur.setRaisonSociale(fournisseurDTO.getRaisonSociale());
    fournisseur.setMatriculeFiscale(fournisseurDTO.getMatriculeFiscale());
    fournisseur.setAdresse(fournisseurDTO.getAdresse());
    fournisseur.setUtilisateur(fournisseurDTO.getUtilisateur());
    Collection<Offre> offreCollections = new ArrayList<>();
    fournisseurDTO.getOffreCollection().forEach(x -> {
      Offre offre = new Offre();
      offre = OffreFactory.offreDTOToOffre(x);
      offreCollections.add(offre);
    } );
    if(fournisseur.getOffreCollection() !=null) {
      fournisseur.getOffreCollection().clear();
      fournisseur.getOffreCollection().addAll(offreCollections);
    }
    else {
      fournisseur.setOffreCollection(offreCollections);
    }
    return fournisseur;
  }

  public static Collection<FournisseurDTO> fournisseurToFournisseurDTOs(Collection<Fournisseur> fournisseurs) {
    List<FournisseurDTO> fournisseursDTO=new ArrayList<>();
    fournisseurs.forEach(x -> {
      fournisseursDTO.add(fournisseurToFournisseurDTO(x));
    } );
    return fournisseursDTO;
  }

  public static FournisseurDTO lazyfournisseurToFournisseurDTO(Fournisseur fournisseur) {
    FournisseurDTO fournisseurDTO=new FournisseurDTO();
    fournisseurDTO.setId(fournisseur.getId());
    fournisseurDTO.setRaisonSociale(fournisseur.getRaisonSociale());
    fournisseurDTO.setMatriculeFiscale(fournisseur.getMatriculeFiscale());
    fournisseurDTO.setAdresse(fournisseur.getAdresse());
    fournisseurDTO.setUtilisateur(fournisseur.getUtilisateur());
    return fournisseurDTO;
  }

  public static Collection<FournisseurDTO> lazyfournisseurToFournisseurDTOs(Collection<Fournisseur> fournisseurs) {
    List<FournisseurDTO> fournisseursDTO=new ArrayList<>();
    fournisseurs.forEach(x -> {
      fournisseursDTO.add(lazyfournisseurToFournisseurDTO(x));
    } );
    return fournisseursDTO;
  }
}

