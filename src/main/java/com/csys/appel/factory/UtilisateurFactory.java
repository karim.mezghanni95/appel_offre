package com.csys.appel.factory;

import com.csys.appel.domain.Fournisseur;
import com.csys.appel.domain.Utilisateur;
import com.csys.appel.dto.FournisseurDTO;
import com.csys.appel.dto.UtilisateurDTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UtilisateurFactory {
    public static UtilisateurDTO utilisateurToUtilisateurDTO(Utilisateur utilisateur) {
        UtilisateurDTO utilisateurDTO = new UtilisateurDTO();
        utilisateurDTO.setLogin(utilisateur.getLogin());
        utilisateurDTO.setMotPasse(utilisateur.getMotPasse());
        utilisateurDTO.setTelephone(utilisateur.getTelephone());
        utilisateurDTO.setEmail(utilisateur.getEmail());
        utilisateurDTO.setRole(utilisateur.getRole());
        Collection<FournisseurDTO> fournisseurCollectionDtos = new ArrayList<>();
        utilisateur.getFournisseurCollection().forEach(x -> {
            FournisseurDTO fournisseurDto = new FournisseurDTO();
            fournisseurDto = FournisseurFactory.fournisseurToFournisseurDTO(x);
            fournisseurCollectionDtos.add(fournisseurDto);
        });
        if (utilisateurDTO.getFournisseurCollection() != null) {
            utilisateurDTO.getFournisseurCollection().clear();
            utilisateurDTO.getFournisseurCollection().addAll(fournisseurCollectionDtos);
        } else {
            utilisateurDTO.setFournisseurCollection(fournisseurCollectionDtos);
        }
        return utilisateurDTO;
    }

    public static Utilisateur utilisateurDTOToUtilisateur(UtilisateurDTO utilisateurDTO) {
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setLogin(utilisateurDTO.getLogin());
        utilisateur.setMotPasse(utilisateurDTO.getMotPasse());
        utilisateur.setTelephone(utilisateurDTO.getTelephone());
        utilisateur.setEmail(utilisateurDTO.getEmail());
        utilisateur.setRole(utilisateurDTO.getRole());
        Collection<Fournisseur> fournisseurCollections = new ArrayList<>();
        utilisateurDTO.getFournisseurCollection().forEach(x -> {
            Fournisseur fournisseur = new Fournisseur();
            fournisseur = FournisseurFactory.fournisseurDTOToFournisseur(x);
            fournisseurCollections.add(fournisseur);
        });
        if (utilisateur.getFournisseurCollection() != null) {
            utilisateur.getFournisseurCollection().clear();
            utilisateur.getFournisseurCollection().addAll(fournisseurCollections);
        } else {
            utilisateur.setFournisseurCollection(fournisseurCollections);
        }
        return utilisateur;
    }

    public static Collection<UtilisateurDTO> utilisateurToUtilisateurDTOs(Collection<Utilisateur> utilisateurs) {
        List<UtilisateurDTO> utilisateursDTO = new ArrayList<>();
        utilisateurs.forEach(x -> {
            utilisateursDTO.add(utilisateurToUtilisateurDTO(x));
        });
        return utilisateursDTO;
    }

    public static UtilisateurDTO lazyutilisateurToUtilisateurDTO(Utilisateur utilisateur) {
        UtilisateurDTO utilisateurDTO = new UtilisateurDTO();
        utilisateurDTO.setLogin(utilisateur.getLogin());
        utilisateurDTO.setMotPasse(utilisateur.getMotPasse());
        utilisateurDTO.setTelephone(utilisateur.getTelephone());
        utilisateurDTO.setEmail(utilisateur.getEmail());
        utilisateurDTO.setRole(utilisateur.getRole());
        return utilisateurDTO;
    }

    public static Collection<UtilisateurDTO> lazyutilisateurToUtilisateurDTOs(Collection<Utilisateur> utilisateurs) {
        List<UtilisateurDTO> utilisateursDTO = new ArrayList<>();
        utilisateurs.forEach(x -> {
            utilisateursDTO.add(lazyutilisateurToUtilisateurDTO(x));
        });
        return utilisateursDTO;
    }
}

