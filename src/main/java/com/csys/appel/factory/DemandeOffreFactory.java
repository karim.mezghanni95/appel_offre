package com.csys.appel.factory;

import com.csys.appel.domain.DemandeOffre;
import com.csys.appel.domain.Offre;
import com.csys.appel.dto.DemandeOffreDTO;
import com.csys.appel.dto.OffreDTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DemandeOffreFactory {
    public static DemandeOffreDTO demandeoffreToDemandeOffreDTO(DemandeOffre demandeoffre) {
        DemandeOffreDTO demandeoffreDTO = new DemandeOffreDTO();
        demandeoffreDTO.setId(demandeoffre.getId());
        demandeoffreDTO.setQunatitDemande(demandeoffre.getQunatitDemande());
        demandeoffreDTO.setAppelOffre(demandeoffre.getAppelOffre());
        demandeoffreDTO.setCategorie(demandeoffre.getCategorie());
        Collection<OffreDTO> offreCollectionDtos = new ArrayList<>();
        demandeoffre.getOffreCollection().forEach(x -> {
            OffreDTO offreDto = new OffreDTO();
            offreDto = OffreFactory.offreToOffreDTO(x);
            offreCollectionDtos.add(offreDto);
        });
        if (demandeoffreDTO.getOffreCollection() != null) {
            demandeoffreDTO.getOffreCollection().clear();
            demandeoffreDTO.getOffreCollection().addAll(offreCollectionDtos);
        } else {
            demandeoffreDTO.setOffreCollection(offreCollectionDtos);
        }
        return demandeoffreDTO;
    }

    public static DemandeOffre demandeoffreDTOToDemandeOffre(DemandeOffreDTO demandeoffreDTO) {
        DemandeOffre demandeoffre = new DemandeOffre();
        demandeoffre.setId(demandeoffreDTO.getId());
        demandeoffre.setQunatitDemande(demandeoffreDTO.getQunatitDemande());
        demandeoffre.setAppelOffre(demandeoffreDTO.getAppelOffre());
        demandeoffre.setCategorie(demandeoffreDTO.getCategorie());
        Collection<Offre> offreCollections = new ArrayList<>();
        demandeoffreDTO.getOffreCollection().forEach(x -> {
            Offre offre = new Offre();
            offre = OffreFactory.offreDTOToOffre(x);
            offreCollections.add(offre);
        });
        if (demandeoffre.getOffreCollection() != null) {
            demandeoffre.getOffreCollection().clear();
            demandeoffre.getOffreCollection().addAll(offreCollections);
        } else {
            demandeoffre.setOffreCollection(offreCollections);
        }
        return demandeoffre;
    }

    public static Collection<DemandeOffreDTO> demandeoffreToDemandeOffreDTOs(Collection<DemandeOffre> demandeoffres) {
        List<DemandeOffreDTO> demandeoffresDTO = new ArrayList<>();
        demandeoffres.forEach(x -> {
            demandeoffresDTO.add(demandeoffreToDemandeOffreDTO(x));
        });
        return demandeoffresDTO;
    }

    public static DemandeOffreDTO lazydemandeoffreToDemandeOffreDTO(DemandeOffre demandeoffre) {
        DemandeOffreDTO demandeoffreDTO = new DemandeOffreDTO();
        demandeoffreDTO.setId(demandeoffre.getId());
        demandeoffreDTO.setQunatitDemande(demandeoffre.getQunatitDemande());
        demandeoffreDTO.setAppelOffre(demandeoffre.getAppelOffre());
        demandeoffreDTO.setCategorie(demandeoffre.getCategorie());
        return demandeoffreDTO;
    }

    public static Collection<DemandeOffreDTO> lazydemandeoffreToDemandeOffreDTOs(Collection<DemandeOffre> demandeoffres) {
        List<DemandeOffreDTO> demandeoffresDTO = new ArrayList<>();
        demandeoffres.forEach(x -> {
            demandeoffresDTO.add(lazydemandeoffreToDemandeOffreDTO(x));
        });
        return demandeoffresDTO;
    }
}

