package com.csys.appel.factory;

import com.csys.appel.domain.Article;
import com.csys.appel.dto.ArticleDTO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ArticleFactory {
  public static ArticleDTO articleToArticleDTO(Article article) {
    ArticleDTO articleDTO=new ArticleDTO();
    articleDTO.setId(article.getId());
    articleDTO.setDesignation(article.getDesignation());
    articleDTO.setActif(article.getActif());
    articleDTO.setCodeSaisi(article.getCodeSaisi());
    articleDTO.setCategorie(article.getCategorie());
    return articleDTO;
  }

  public static Article articleDTOToArticle(ArticleDTO articleDTO) {
    Article article=new Article();
    article.setId(articleDTO.getId());
    article.setDesignation(articleDTO.getDesignation());
    article.setActif(articleDTO.getActif());
    article.setCodeSaisi(articleDTO.getCodeSaisi());
    article.setCategorie(articleDTO.getCategorie());
    return article;
  }

  public static Collection<ArticleDTO> articleToArticleDTOs(Collection<Article> articles) {
    List<ArticleDTO> articlesDTO=new ArrayList<>();
    articles.forEach(x -> {
      articlesDTO.add(articleToArticleDTO(x));
    } );
    return articlesDTO;
  }
}

