package com.csys.appel.factory;

import com.csys.appel.domain.Article;
import com.csys.appel.domain.Categorie;
import com.csys.appel.domain.DemandeOffre;
import com.csys.appel.dto.ArticleDTO;
import com.csys.appel.dto.CategorieDTO;
import com.csys.appel.dto.DemandeOffreDTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CategorieFactory {
    public static CategorieDTO categorieToCategorieDTO(Categorie categorie) {
        CategorieDTO categorieDTO = new CategorieDTO();
        categorieDTO.setId(categorie.getId());
        categorieDTO.setDesignation(categorie.getDesignation());
        categorieDTO.setActif(categorie.getActif());
        Collection<ArticleDTO> articleCollectionDtos = new ArrayList<>();
        categorie.getArticleCollection().forEach(x -> {
            ArticleDTO articleDto = new ArticleDTO();
            articleDto = ArticleFactory.articleToArticleDTO(x);
            articleCollectionDtos.add(articleDto);
        });
        if (categorieDTO.getArticleCollection() != null) {
            categorieDTO.getArticleCollection().clear();
            categorieDTO.getArticleCollection().addAll(articleCollectionDtos);
        } else {
            categorieDTO.setArticleCollection(articleCollectionDtos);
        }
        Collection<DemandeOffreDTO> demandeOffreCollectionDtos = new ArrayList<>();
        categorie.getDemandeOffreCollection().forEach(x -> {
            DemandeOffreDTO demandeoffreDto = new DemandeOffreDTO();
            demandeoffreDto = DemandeOffreFactory.demandeoffreToDemandeOffreDTO(x);
            demandeOffreCollectionDtos.add(demandeoffreDto);
        });
        if (categorieDTO.getDemandeOffreCollection() != null) {
            categorieDTO.getDemandeOffreCollection().clear();
            categorieDTO.getDemandeOffreCollection().addAll(demandeOffreCollectionDtos);
        } else {
            categorieDTO.setDemandeOffreCollection(demandeOffreCollectionDtos);
        }
        return categorieDTO;
    }

    public static Categorie categorieDTOToCategorie(CategorieDTO categorieDTO) {
        Categorie categorie = new Categorie();
        categorie.setId(categorieDTO.getId());
        categorie.setDesignation(categorieDTO.getDesignation());
        categorie.setActif(categorieDTO.getActif());
        Collection<Article> articleCollections = new ArrayList<>();
        categorieDTO.getArticleCollection().forEach(x -> {
            Article article = new Article();
            article = ArticleFactory.articleDTOToArticle(x);
            articleCollections.add(article);
        });
        if (categorie.getArticleCollection() != null) {
            categorie.getArticleCollection().clear();
            categorie.getArticleCollection().addAll(articleCollections);
        } else {
            categorie.setArticleCollection(articleCollections);
        }
        Collection<DemandeOffre> demandeOffreCollections = new ArrayList<>();
        categorieDTO.getDemandeOffreCollection().forEach(x -> {
            DemandeOffre demandeoffre = new DemandeOffre();
            demandeoffre = DemandeOffreFactory.demandeoffreDTOToDemandeOffre(x);
            demandeOffreCollections.add(demandeoffre);
        });
        if (categorie.getDemandeOffreCollection() != null) {
            categorie.getDemandeOffreCollection().clear();
            categorie.getDemandeOffreCollection().addAll(demandeOffreCollections);
        } else {
            categorie.setDemandeOffreCollection(demandeOffreCollections);
        }
        return categorie;
    }

    public static Collection<CategorieDTO> categorieToCategorieDTOs(Collection<Categorie> categories) {
        List<CategorieDTO> categoriesDTO = new ArrayList<>();
        categories.forEach(x -> {
            categoriesDTO.add(categorieToCategorieDTO(x));
        });
        return categoriesDTO;
    }

    public static CategorieDTO lazycategorieToCategorieDTO(Categorie categorie) {
        CategorieDTO categorieDTO = new CategorieDTO();
        categorieDTO.setId(categorie.getId());
        categorieDTO.setDesignation(categorie.getDesignation());
        categorieDTO.setActif(categorie.getActif());
        return categorieDTO;
    }

    public static Collection<CategorieDTO> lazycategorieToCategorieDTOs(Collection<Categorie> categories) {
        List<CategorieDTO> categoriesDTO = new ArrayList<>();
        categories.forEach(x -> {
            categoriesDTO.add(lazycategorieToCategorieDTO(x));
        });
        return categoriesDTO;
    }


}

