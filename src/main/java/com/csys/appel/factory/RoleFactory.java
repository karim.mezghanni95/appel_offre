package com.csys.appel.factory;

import com.csys.appel.domain.Role;
import com.csys.appel.domain.Utilisateur;
import com.csys.appel.dto.RoleDTO;
import com.csys.appel.dto.UtilisateurDTO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class RoleFactory {
  public static RoleDTO roleToRoleDTO(Role role) {
    RoleDTO roleDTO=new RoleDTO();
    roleDTO.setId(role.getId());
    roleDTO.setDesignation(role.getDesignation());
    Collection<UtilisateurDTO> utilisateurCollectionDtos = new ArrayList<>();
    role.getUtilisateurCollection().forEach(x -> {
      UtilisateurDTO utilisateurDto = new UtilisateurDTO();
      utilisateurDto = UtilisateurFactory.utilisateurToUtilisateurDTO(x);
      utilisateurCollectionDtos.add(utilisateurDto);
    } );
    if(roleDTO.getUtilisateurCollection() !=null) {
      roleDTO.getUtilisateurCollection().clear();
      roleDTO.getUtilisateurCollection().addAll(utilisateurCollectionDtos);
    }
    else {
      roleDTO.setUtilisateurCollection(utilisateurCollectionDtos);
    }
    return roleDTO;
  }

  public static Role roleDTOToRole(RoleDTO roleDTO) {
    Role role=new Role();
    role.setId(roleDTO.getId());
    role.setDesignation(roleDTO.getDesignation());
    Collection<Utilisateur> utilisateurCollections = new ArrayList<>();
    roleDTO.getUtilisateurCollection().forEach(x -> {
      Utilisateur utilisateur = new Utilisateur();
      utilisateur = UtilisateurFactory.utilisateurDTOToUtilisateur(x);
      utilisateurCollections.add(utilisateur);
    } );
    if(role.getUtilisateurCollection() !=null) {
      role.getUtilisateurCollection().clear();
      role.getUtilisateurCollection().addAll(utilisateurCollections);
    }
    else {
      role.setUtilisateurCollection(utilisateurCollections);
    }
    return role;
  }

  public static Collection<RoleDTO> roleToRoleDTOs(Collection<Role> roles) {
    List<RoleDTO> rolesDTO=new ArrayList<>();
    roles.forEach(x -> {
      rolesDTO.add(roleToRoleDTO(x));
    } );
    return rolesDTO;
  }

  public static RoleDTO lazyroleToRoleDTO(Role role) {
    RoleDTO roleDTO=new RoleDTO();
    roleDTO.setId(role.getId());
    roleDTO.setDesignation(role.getDesignation());
    return roleDTO;
  }

  public static Collection<RoleDTO> lazyroleToRoleDTOs(Collection<Role> roles) {
    List<RoleDTO> rolesDTO=new ArrayList<>();
    roles.forEach(x -> {
      rolesDTO.add(lazyroleToRoleDTO(x));
    } );
    return rolesDTO;
  }
}

