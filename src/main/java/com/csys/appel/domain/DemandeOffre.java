/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.csys.appel.domain;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Administrateur
 */
@Entity
@Table(name = "demande_offre")
@NamedQueries({
        @NamedQuery(name = "DemandeOffre.findAll", query = "SELECT d FROM DemandeOffre d")})
public class DemandeOffre implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "qunatit_demande")
    private Integer qunatitDemande;
    @JoinColumn(name = "id_appel_offre", referencedColumnName = "id")
    @ManyToOne
    private AppelOffre appelOffre;
    @JoinColumn(name = "id_artcle", referencedColumnName = "id")
    @ManyToOne
    private Categorie categorie;
    @OneToMany(mappedBy = "demandeOffre")
    private Collection<Offre> offreCollection;

    public DemandeOffre() {
    }

    public DemandeOffre(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQunatitDemande() {
        return qunatitDemande;
    }

    public void setQunatitDemande(Integer qunatitDemande) {
        this.qunatitDemande = qunatitDemande;
    }

    public AppelOffre getAppelOffre() {
        return appelOffre;
    }

    public void setAppelOffre(AppelOffre appelOffre) {
        this.appelOffre = appelOffre;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Collection<Offre> getOffreCollection() {
        return offreCollection;
    }

    public void setOffreCollection(Collection<Offre> offreCollection) {
        this.offreCollection = offreCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DemandeOffre)) {
            return false;
        }
        DemandeOffre other = (DemandeOffre) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.csys.appel.domain.DemandeOffre[ id=" + id + " ]";
    }

}
