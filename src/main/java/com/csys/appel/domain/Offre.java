/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.csys.appel.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Administrateur
 */
@Entity
@Table(name = "offre")
@NamedQueries({
        @NamedQuery(name = "Offre.findAll", query = "SELECT o FROM Offre o")})
public class Offre implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id", nullable = false)
    private Integer id;
    @Size(max = 100)
    @Column(name = "unite_mesure", length = 100)
    private String uniteMesure;
    @Size(max = 100)
    @Column(name = "marque", length = 100)
    private String marque;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "prix_unitaire", precision = 18, scale = 3)
    private BigDecimal prixUnitaire;
    @Size(max = 100)
    @Column(name = "origine", length = 100)
    private String origine;
    @Column(name = "delai_livraison")
    private Integer delaiLivraison;
    @Size(max = 255)
    @Column(name = "observation", length = 255)
    private String observation;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amc", nullable = false)
    private boolean amc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "echantillon", nullable = false)
    private boolean echantillon;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fodec", nullable = false)
    private boolean fodec;
    @Basic(optional = false)
    @NotNull
    @Column(name = "avec_code_barre", nullable = false)
    private boolean avecCodeBarre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "conditionnement", nullable = false, length = 100)
    private String conditionnement;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prix_conditionnement", nullable = false, precision = 18, scale = 3)
    private BigDecimal prixConditionnement;
    @JoinColumn(name = "id_demande_offre", referencedColumnName = "id")
    @ManyToOne
    private DemandeOffre demandeOffre;
    @JoinColumn(name = "id_fournisseur", referencedColumnName = "id")
    @ManyToOne
    private Fournisseur fournisseur;
    @JoinColumn(name = "id_tva", referencedColumnName = "id")
    @ManyToOne
    private Tva tva;

    public Offre() {
    }

    public Offre(Integer id) {
        this.id = id;
    }

    public Offre(Integer id, boolean amc, boolean echantillon, boolean fodec, boolean avecCodeBarre, String conditionnement, BigDecimal prixConditionnement) {
        this.id = id;
        this.amc = amc;
        this.echantillon = echantillon;
        this.fodec = fodec;
        this.avecCodeBarre = avecCodeBarre;
        this.conditionnement = conditionnement;
        this.prixConditionnement = prixConditionnement;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUniteMesure() {
        return uniteMesure;
    }

    public void setUniteMesure(String uniteMesure) {
        this.uniteMesure = uniteMesure;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public BigDecimal getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public String getOrigine() {
        return origine;
    }

    public void setOrigine(String origine) {
        this.origine = origine;
    }

    public Integer getDelaiLivraison() {
        return delaiLivraison;
    }

    public void setDelaiLivraison(Integer delaiLivraison) {
        this.delaiLivraison = delaiLivraison;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public boolean getAmc() {
        return amc;
    }

    public void setAmc(boolean amc) {
        this.amc = amc;
    }

    public boolean getEchantillon() {
        return echantillon;
    }

    public void setEchantillon(boolean echantillon) {
        this.echantillon = echantillon;
    }

    public boolean getFodec() {
        return fodec;
    }

    public void setFodec(boolean fodec) {
        this.fodec = fodec;
    }

    public boolean getAvecCodeBarre() {
        return avecCodeBarre;
    }

    public void setAvecCodeBarre(boolean avecCodeBarre) {
        this.avecCodeBarre = avecCodeBarre;
    }

    public String getConditionnement() {
        return conditionnement;
    }

    public void setConditionnement(String conditionnement) {
        this.conditionnement = conditionnement;
    }

    public BigDecimal getPrixConditionnement() {
        return prixConditionnement;
    }

    public void setPrixConditionnement(BigDecimal prixConditionnement) {
        this.prixConditionnement = prixConditionnement;
    }

    public DemandeOffre getDemandeOffre() {
        return demandeOffre;
    }

    public void setDemandeOffre(DemandeOffre demandeOffre) {
        this.demandeOffre = demandeOffre;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public Tva getTva() {
        return tva;
    }

    public void setTva(Tva tva) {
        this.tva = tva;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Offre)) {
            return false;
        }
        Offre other = (Offre) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.csys.appel.domain.Offre[ id=" + id + " ]";
    }

}
