package com.csys.appel.dto;

import com.csys.appel.domain.AppelOffre;
import com.csys.appel.domain.Categorie;

import java.lang.Integer;
import java.util.Collection;
import javax.validation.constraints.NotNull;

public class DemandeOffreDTO {
    @NotNull
    private Integer id;

    private Integer qunatitDemande;

    private AppelOffre appelOffre;

    private Categorie categorie;

    private Collection<OffreDTO> offreCollection;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQunatitDemande() {
        return qunatitDemande;
    }

    public void setQunatitDemande(Integer qunatitDemande) {
        this.qunatitDemande = qunatitDemande;
    }

    public AppelOffre getAppelOffre() {
        return appelOffre;
    }

    public void setAppelOffre(AppelOffre appelOffre) {
        this.appelOffre = appelOffre;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Collection<OffreDTO> getOffreCollection() {
        return offreCollection;
    }

    public void setOffreCollection(Collection<OffreDTO> offreCollection) {
        this.offreCollection = offreCollection;
    }
}

