package com.csys.appel.dto;

import com.csys.appel.domain.Utilisateur;
import java.lang.Integer;
import java.lang.String;
import java.util.Collection;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class FournisseurDTO {
  @NotNull
  private Integer id;

  @Size(
      min = 0,
      max = 100
  )
  private String raisonSociale;

  @Size(
      min = 0,
      max = 50
  )
  private String matriculeFiscale;

  @Size(
      min = 0,
      max = 200
  )
  private String adresse;

  private Utilisateur utilisateur;

  private Collection<OffreDTO> offreCollection;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getRaisonSociale() {
    return raisonSociale;
  }

  public void setRaisonSociale(String raisonSociale) {
    this.raisonSociale = raisonSociale;
  }

  public String getMatriculeFiscale() {
    return matriculeFiscale;
  }

  public void setMatriculeFiscale(String matriculeFiscale) {
    this.matriculeFiscale = matriculeFiscale;
  }

  public String getAdresse() {
    return adresse;
  }

  public void setAdresse(String adresse) {
    this.adresse = adresse;
  }

  public Utilisateur getUtilisateur() {
    return utilisateur;
  }

  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }

  public Collection<OffreDTO> getOffreCollection() {
    return offreCollection;
  }

  public void setOffreCollection(Collection<OffreDTO> offreCollection) {
    this.offreCollection = offreCollection;
  }
}

