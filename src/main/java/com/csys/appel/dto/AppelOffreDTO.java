package com.csys.appel.dto;

import java.lang.Integer;
import java.lang.String;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AppelOffreDTO {
    @NotNull
    private Integer id;

    @Size(
            min = 0,
            max = 50
    )
    private String numero;

    @Temporal(TemporalType.DATE)
    private Date dateDebut;

    @Temporal(TemporalType.DATE)
    private Date dateFin;

    @Size(
            min = 0,
            max = 4
    )
    private String exercice;

    @Size(
            min = 0,
            max = 255
    )
    private String designation;

    private Collection<DemandeOffreDTO> demandeOffreCollection;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getExercice() {
        return exercice;
    }

    public void setExercice(String exercice) {
        this.exercice = exercice;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Collection<DemandeOffreDTO> getDemandeOffreCollection() {
        return demandeOffreCollection;
    }

    public void setDemandeOffreCollection(Collection<DemandeOffreDTO> demandeOffreCollection) {
        this.demandeOffreCollection = demandeOffreCollection;
    }
}

