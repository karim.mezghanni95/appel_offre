package com.csys.appel.dto;

import java.lang.Integer;
import java.lang.String;
import java.util.Collection;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CategorieDTO {
  @NotNull
  private Integer id;

  @Size(
      min = 0,
      max = 100
  )
  private String designation;

  @NotNull
  private boolean actif;

  private Collection<ArticleDTO> articleCollection;

  private Collection<DemandeOffreDTO> demandeOffreCollection;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getDesignation() {
    return designation;
  }

  public void setDesignation(String designation) {
    this.designation = designation;
  }

  public boolean getActif() {
    return actif;
  }

  public void setActif(boolean actif) {
    this.actif = actif;
  }

  public Collection<ArticleDTO> getArticleCollection() {
    return articleCollection;
  }

  public void setArticleCollection(Collection<ArticleDTO> articleCollection) {
    this.articleCollection = articleCollection;
  }

  public Collection<DemandeOffreDTO> getDemandeOffreCollection() {
    return demandeOffreCollection;
  }

  public void setDemandeOffreCollection(Collection<DemandeOffreDTO> demandeOffreCollection) {
    this.demandeOffreCollection = demandeOffreCollection;
  }
}

