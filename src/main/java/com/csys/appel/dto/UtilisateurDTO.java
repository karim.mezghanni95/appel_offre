package com.csys.appel.dto;

import com.csys.appel.domain.Role;
import java.lang.String;
import java.util.Collection;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UtilisateurDTO {
  @NotNull
  @Size(
      min = 1,
      max = 20
  )
  private String login;

  @Size(
      min = 0,
      max = 50
  )
  private String motPasse;

  @Size(
      min = 0,
      max = 50
  )
  private String telephone;

  @Size(
      min = 0,
      max = 100
  )
  private String email;

  private Role role;

  private Collection<FournisseurDTO> fournisseurCollection;

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getMotPasse() {
    return motPasse;
  }

  public void setMotPasse(String motPasse) {
    this.motPasse = motPasse;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public Collection<FournisseurDTO> getFournisseurCollection() {
    return fournisseurCollection;
  }

  public void setFournisseurCollection(Collection<FournisseurDTO> fournisseurCollection) {
    this.fournisseurCollection = fournisseurCollection;
  }
}

