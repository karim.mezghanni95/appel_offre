package com.csys.appel.dto;

import java.lang.Integer;
import java.lang.String;
import java.util.Collection;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RoleDTO {
  @NotNull
  private Integer id;

  @Size(
      min = 0,
      max = 50
  )
  private String designation;

  private Collection<UtilisateurDTO> utilisateurCollection;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getDesignation() {
    return designation;
  }

  public void setDesignation(String designation) {
    this.designation = designation;
  }

  public Collection<UtilisateurDTO> getUtilisateurCollection() {
    return utilisateurCollection;
  }

  public void setUtilisateurCollection(Collection<UtilisateurDTO> utilisateurCollection) {
    this.utilisateurCollection = utilisateurCollection;
  }
}

