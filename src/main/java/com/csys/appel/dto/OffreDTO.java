package com.csys.appel.dto;

import com.csys.appel.domain.DemandeOffre;
import com.csys.appel.domain.Fournisseur;
import com.csys.appel.domain.Tva;

import java.lang.Integer;
import java.lang.String;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class OffreDTO {
    @NotNull
    private Integer id;

    @Size(
            min = 0,
            max = 100
    )
    private String uniteMesure;

    @Size(
            min = 0,
            max = 100
    )
    private String marque;

    private BigDecimal prixUnitaire;

    @Size(
            min = 0,
            max = 100
    )
    private String origine;

    private Integer delaiLivraison;

    @Size(
            min = 0,
            max = 255
    )
    private String observation;

    @NotNull
    private boolean amc;

    @NotNull
    private boolean echantillon;

    @NotNull
    private boolean fodec;

    @NotNull
    private boolean avecCodeBarre;

    @NotNull
    @Size(
            min = 1,
            max = 100
    )
    private String conditionnement;

    @NotNull
    private BigDecimal prixConditionnement;

    private DemandeOffre demandeOffre;

    private Fournisseur fournisseur;

    private Tva tva;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUniteMesure() {
        return uniteMesure;
    }

    public void setUniteMesure(String uniteMesure) {
        this.uniteMesure = uniteMesure;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public BigDecimal getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public String getOrigine() {
        return origine;
    }

    public void setOrigine(String origine) {
        this.origine = origine;
    }

    public Integer getDelaiLivraison() {
        return delaiLivraison;
    }

    public void setDelaiLivraison(Integer delaiLivraison) {
        this.delaiLivraison = delaiLivraison;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public boolean getAmc() {
        return amc;
    }

    public void setAmc(boolean amc) {
        this.amc = amc;
    }

    public boolean getEchantillon() {
        return echantillon;
    }

    public void setEchantillon(boolean echantillon) {
        this.echantillon = echantillon;
    }

    public boolean getFodec() {
        return fodec;
    }

    public void setFodec(boolean fodec) {
        this.fodec = fodec;
    }

    public boolean getAvecCodeBarre() {
        return avecCodeBarre;
    }

    public void setAvecCodeBarre(boolean avecCodeBarre) {
        this.avecCodeBarre = avecCodeBarre;
    }

    public String getConditionnement() {
        return conditionnement;
    }

    public void setConditionnement(String conditionnement) {
        this.conditionnement = conditionnement;
    }

    public BigDecimal getPrixConditionnement() {
        return prixConditionnement;
    }

    public void setPrixConditionnement(BigDecimal prixConditionnement) {
        this.prixConditionnement = prixConditionnement;
    }

    public DemandeOffre getDemandeOffre() {
        return demandeOffre;
    }

    public void setDemandeOffre(DemandeOffre demandeOffre) {
        this.demandeOffre = demandeOffre;
    }

    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    public Tva getTva() {
        return tva;
    }

    public void setTva(Tva tva) {
        this.tva = tva;
    }
}

