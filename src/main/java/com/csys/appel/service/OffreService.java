package com.csys.appel.service;

import com.csys.appel.domain.Offre;
import com.csys.appel.dto.OffreDTO;
import com.csys.appel.factory.OffreFactory;
import com.csys.appel.repository.OffreRepository;
import com.google.common.base.Preconditions;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Offre.
 */
@Service
@Transactional
public class OffreService {
  private final Logger log = LoggerFactory.getLogger(OffreService.class);

  private final OffreRepository offreRepository;

  public OffreService(OffreRepository offreRepository) {
    this.offreRepository=offreRepository;
  }

  /**
   * Save a offreDTO.
   *
   * @param offreDTO
   * @return the persisted entity
   */
  public OffreDTO save(OffreDTO offreDTO) {
    log.debug("Request to save Offre: {}",offreDTO);
    Offre offre = OffreFactory.offreDTOToOffre(offreDTO);
    offre = offreRepository.save(offre);
    OffreDTO resultDTO = OffreFactory.offreToOffreDTO(offre);
    return resultDTO;
  }

  /**
   * Update a offreDTO.
   *
   * @param offreDTO
   * @return the updated entity
   */
  public OffreDTO update(OffreDTO offreDTO) {
    log.debug("Request to update Offre: {}",offreDTO);
    Offre offre = offreRepository.findById(offreDTO.getId())
            .orElseThrow(() -> new IllegalArgumentException("Offre not found"));
    Offre updatedOffre = OffreFactory.offreDTOToOffre(offreDTO);
    updatedOffre.setId(offre.getId()); // Ensure the ID remains the same
    updatedOffre = offreRepository.save(updatedOffre);
    return OffreFactory.offreToOffreDTO(updatedOffre);
  }

  /**
   * Get one offreDTO by id.
   *
   * @param id the id of the entity
   * @return the entity DTO
   */
  @Transactional(readOnly = true)
  public OffreDTO findById(Integer id) {
    log.debug("Request to get Offre: {}",id);
    Offre offre= offreRepository.findById(id)
            .orElse(null);
    return OffreFactory.offreToOffreDTO(offre);
  }

  /**
   * Get one offre by id.
   *
   * @param id the id of the entity
   * @return the entity
   */
  @Transactional(readOnly = true)
  public Offre findOffre(Integer id) {
    log.debug("Request to get Offre: {}",id);
    return offreRepository.findById(id)
            .orElse(null);
  }

  /**
   * Get all the offres.
   *
   * @return the list of entities
   */
  @Transactional(readOnly = true)
  public Collection<OffreDTO> findAll() {
    log.debug("Request to get All Offres");
    Collection<Offre> result= offreRepository.findAll();
    return OffreFactory.offreToOffreDTOs(result);
  }

  /**
   * Delete offre by id.
   *
   * @param id the id of the entity
   */
  public void delete(Integer id) {
    log.debug("Request to delete Offre: {}",id);
    offreRepository.deleteById(id);
  }
}
