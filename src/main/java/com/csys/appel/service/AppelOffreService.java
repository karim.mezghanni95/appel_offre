package com.csys.appel.service;

import com.csys.appel.domain.AppelOffre;
import com.csys.appel.dto.AppelOffreDTO;
import com.csys.appel.factory.AppelOffreFactory;
import com.csys.appel.repository.AppelOffreRepository;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AppelOffreService {
    private final Logger log = LoggerFactory.getLogger(AppelOffreService.class);

    private final AppelOffreRepository appeloffreRepository;

    public AppelOffreService(AppelOffreRepository appeloffreRepository) {
        this.appeloffreRepository = appeloffreRepository;
    }

    public AppelOffreDTO save(AppelOffreDTO appeloffreDTO) {
        log.debug("Request to save AppelOffre: {}", appeloffreDTO);
        AppelOffre appeloffre = AppelOffreFactory.appeloffreDTOToAppelOffre(appeloffreDTO);
        appeloffre = appeloffreRepository.save(appeloffre);
        return AppelOffreFactory.appeloffreToAppelOffreDTO(appeloffre);
    }

    public AppelOffreDTO update(AppelOffreDTO appeloffreDTO) {
        log.debug("Request to update AppelOffre: {}", appeloffreDTO);
        AppelOffre appeloffre = appeloffreRepository.findById(appeloffreDTO.getId())
                .orElseThrow(() -> new IllegalArgumentException("AppelOffre not found"));
        AppelOffre updatedAppelOffre = AppelOffreFactory.appeloffreDTOToAppelOffre(appeloffreDTO);
        updatedAppelOffre.setId(appeloffre.getId()); // Ensure the ID remains the same
        updatedAppelOffre = appeloffreRepository.save(updatedAppelOffre);
        return AppelOffreFactory.appeloffreToAppelOffreDTO(updatedAppelOffre);
    }

    @Transactional(readOnly = true)
    public AppelOffreDTO findById(Integer id) {
        log.debug("Request to get AppelOffre: {}", id);
        AppelOffre appeloffre = appeloffreRepository.findById(id)
                .orElse(null);
        return AppelOffreFactory.appeloffreToAppelOffreDTO(appeloffre);
    }

    @Transactional(readOnly = true)
    public AppelOffre findAppelOffre(Integer id) {
        log.debug("Request to get AppelOffre: {}", id);
        return appeloffreRepository.findById(id)
                .orElse(null);
    }

    @Transactional(readOnly = true)
    public Collection<AppelOffreDTO> findAll() {
        log.debug("Request to get All AppelOffres");
        Collection<AppelOffre> result = appeloffreRepository.findAll();
        return AppelOffreFactory.appeloffreToAppelOffreDTOs(result);
    }

    public void delete(Integer id) {
        log.debug("Request to delete AppelOffre: {}", id);
        appeloffreRepository.deleteById(id);
    }
}
