package com.csys.appel.service;

import com.csys.appel.domain.Article;
import com.csys.appel.dto.ArticleDTO;
import com.csys.appel.factory.ArticleFactory;
import com.csys.appel.repository.ArticleRepository;
import com.google.common.base.Preconditions;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Article.
 */
@Service
@Transactional
public class ArticleService {
    private final Logger log = LoggerFactory.getLogger(ArticleService.class);

    private final ArticleRepository articleRepository;

    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    /**
     * Save a articleDTO.
     *
     * @param articleDTO
     * @return the persisted entity
     */
    public ArticleDTO save(ArticleDTO articleDTO) {
        log.debug("Request to save Article: {}", articleDTO);
        Article article = ArticleFactory.articleDTOToArticle(articleDTO);
        article = articleRepository.save(article);
        ArticleDTO resultDTO = ArticleFactory.articleToArticleDTO(article);
        return resultDTO;
    }

    /**
     * Update a articleDTO.
     *
     * @param articleDTO
     * @return the updated entity
     */
    public ArticleDTO update(ArticleDTO articleDTO) {
        log.debug("Request to update Article: {}", articleDTO);
        Article inBase = articleRepository.findById(articleDTO.getId()).orElse(null);
        Preconditions.checkArgument(inBase != null, "article.NotFound");
        Article article = ArticleFactory.articleDTOToArticle(articleDTO);
        article = articleRepository.save(article);
        ArticleDTO resultDTO = ArticleFactory.articleToArticleDTO(article);
        return resultDTO;
    }

    /**
     * Get one articleDTO by id.
     *
     * @param id the id of the entity
     * @return the entity DTO
     */
    @Transactional(
            readOnly = true
    )
    public ArticleDTO findOne(Integer id) {
        log.debug("Request to get Article: {}", id);
        Article article = articleRepository.findById(id).orElse(null);
        return ArticleFactory.articleToArticleDTO(article);
    }

    /**
     * Get one article by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(
            readOnly = true
    )
    public Article findArticle(Integer id) {
        log.debug("Request to get Article: {}", id);
        return articleRepository.findById(id).orElse(null);
    }

    /**
     * Get all the articles.
     *
     * @return the the list of entities
     */
    @Transactional(
            readOnly = true
    )
    public Collection<ArticleDTO> findAll() {
        log.debug("Request to get All Articles");
        Collection<Article> result = articleRepository.findAll();
        return ArticleFactory.articleToArticleDTOs(result);
    }

    /**
     * Delete article by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete Article: {}", id);
        articleRepository.deleteById(id);
    }
}
