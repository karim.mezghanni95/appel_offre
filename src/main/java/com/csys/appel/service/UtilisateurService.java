package com.csys.appel.service;

import com.csys.appel.domain.Utilisateur;
import com.csys.appel.dto.UtilisateurDTO;
import com.csys.appel.factory.UtilisateurFactory;
import com.csys.appel.repository.UtilisateurRepository;
import com.google.common.base.Preconditions;

import java.lang.String;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Utilisateur.
 */
@Service
@Transactional
public class UtilisateurService {
    private final Logger log = LoggerFactory.getLogger(UtilisateurService.class);

    private final UtilisateurRepository utilisateurRepository;

    public UtilisateurService(UtilisateurRepository utilisateurRepository) {
        this.utilisateurRepository = utilisateurRepository;
    }

    /**
     * Save a utilisateurDTO.
     *
     * @param utilisateurDTO
     * @return the persisted entity
     */
    public UtilisateurDTO save(UtilisateurDTO utilisateurDTO) {
        log.debug("Request to save Utilisateur: {}", utilisateurDTO);
        Utilisateur utilisateur = UtilisateurFactory.utilisateurDTOToUtilisateur(utilisateurDTO);
        utilisateur = utilisateurRepository.save(utilisateur);
        UtilisateurDTO resultDTO = UtilisateurFactory.utilisateurToUtilisateurDTO(utilisateur);
        return resultDTO;
    }

    /**
     * Update a utilisateurDTO.
     *
     * @param utilisateurDTO
     * @return the updated entity
     */
    public UtilisateurDTO update(UtilisateurDTO utilisateurDTO) {
        log.debug("Request to update Utilisateur: {}", utilisateurDTO);
        Utilisateur utilisateur = utilisateurRepository.findById(utilisateurDTO.getLogin()).orElseThrow(() -> new IllegalArgumentException());
        Utilisateur updateUtilisateur = UtilisateurFactory.utilisateurDTOToUtilisateur(utilisateurDTO);
        updateUtilisateur.setLogin(utilisateur.getLogin());
        updateUtilisateur = utilisateurRepository.save(updateUtilisateur);
        return UtilisateurFactory.utilisateurToUtilisateurDTO(updateUtilisateur);


    }


    /**
     * Get one utilisateurDTO by id.
     *
     * @param id the id of the entity
     * @return the entity DTO
     */
    @Transactional(
            readOnly = true
    )
    public UtilisateurDTO findById(String id) {
        log.debug("Request to get Utilisateur: {}", id);
        Utilisateur utilisateur = utilisateurRepository.findById(id).orElse(null);
        UtilisateurDTO dto = UtilisateurFactory.utilisateurToUtilisateurDTO(utilisateur);
        return dto;
    }

    /**
     * Get one utilisateur by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(
            readOnly = true
    )
    public Utilisateur findUtilisateur(String id) {
        log.debug("Request to get Utilisateur: {}", id);

        return utilisateurRepository.findById(id).orElse(null);
    }

    /**
     * Get all the utilisateurs.
     *
     * @return the the list of entities
     */
    @Transactional(
            readOnly = true
    )
    public Collection<UtilisateurDTO> findAll() {
        log.debug("Request to get All Utilisateurs");
        Collection<Utilisateur> result = utilisateurRepository.findAll();
        return UtilisateurFactory.utilisateurToUtilisateurDTOs(result);
    }

    /**
     * Delete utilisateur by id.
     *
     * @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Utilisateur: {}", id);
        utilisateurRepository.deleteById(id);
    }
}

