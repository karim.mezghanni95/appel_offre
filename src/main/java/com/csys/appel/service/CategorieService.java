package com.csys.appel.service;

import com.csys.appel.domain.Categorie;
import com.csys.appel.dto.CategorieDTO;
import com.csys.appel.factory.CategorieFactory;
import com.csys.appel.repository.CategorieRepository;
import com.google.common.base.Preconditions;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Categorie.
 */
@Service
@Transactional
public class CategorieService {
    private final Logger log = LoggerFactory.getLogger(CategorieService.class);

    private final CategorieRepository categorieRepository;

    public CategorieService(CategorieRepository categorieRepository) {
        this.categorieRepository = categorieRepository;
    }

    /**
     * Save a categorieDTO.
     *
     * @param categorieDTO
     * @return the persisted entity
     */
    public CategorieDTO save(CategorieDTO categorieDTO) {
        log.debug("Request to save Categorie: {}", categorieDTO);
        Categorie categorie = CategorieFactory.categorieDTOToCategorie(categorieDTO);
        categorie = categorieRepository.save(categorie);
        CategorieDTO resultDTO = CategorieFactory.categorieToCategorieDTO(categorie);
        return resultDTO;
    }

    /**
     * Update a categorieDTO.
     *
     * @param categorieDTO
     * @return the updated entity
     */
    public CategorieDTO update(CategorieDTO categorieDTO) {
        log.debug("Request to update Categorie: {}", categorieDTO);
        Categorie categorie = categorieRepository.findById(categorieDTO.getId())
                .orElseThrow(() -> new IllegalArgumentException("Categorie not found"));
        Categorie updatedCategorie = CategorieFactory.categorieDTOToCategorie(categorieDTO);
        updatedCategorie.setId(categorie.getId()); // Ensure the ID remains the same
        updatedCategorie = categorieRepository.save(updatedCategorie);
        return CategorieFactory.categorieToCategorieDTO(updatedCategorie);
    }

    /**
     * Get one categorieDTO by id.
     *
     * @param id the id of the entity
     * @return the entity DTO
     */
    @Transactional(readOnly = true)
    public CategorieDTO findById(Integer id) {
        log.debug("Request to get Categorie: {}", id);
        Categorie categorie = categorieRepository.findById(id)
                .orElse(null);
        return CategorieFactory.categorieToCategorieDTO(categorie);
    }

    /**
     * Get one categorie by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Categorie findCategorie(Integer id) {
        log.debug("Request to get Categorie: {}", id);
        return categorieRepository.findById(id)
                .orElse(null);
    }

    /**
     * Get all the categories.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Collection<CategorieDTO> findAll() {
        log.debug("Request to get All Categories");
        Collection<Categorie> result = categorieRepository.findAll();
        return CategorieFactory.categorieToCategorieDTOs(result);
    }

    /**
     * Delete categorie by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete Categorie: {}", id);
        categorieRepository.deleteById(id);
    }
}
