package com.csys.appel.service;

import com.csys.appel.domain.Fournisseur;
import com.csys.appel.dto.FournisseurDTO;
import com.csys.appel.factory.FournisseurFactory;
import com.csys.appel.repository.FournisseurRepository;
import com.google.common.base.Preconditions;

import java.lang.Integer;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Fournisseur.
 */
@Service
@Transactional
public class FournisseurService {
    private final Logger log = LoggerFactory.getLogger(FournisseurService.class);

    private final FournisseurRepository fournisseurRepository;

    public FournisseurService(FournisseurRepository fournisseurRepository) {
        this.fournisseurRepository = fournisseurRepository;
    }

    /**
     * Save a fournisseurDTO.
     *
     * @param fournisseurDTO
     * @return the persisted entity
     */
    public FournisseurDTO save(FournisseurDTO fournisseurDTO) {
        log.debug("Request to save Fournisseur: {}", fournisseurDTO);
        Fournisseur fournisseur = FournisseurFactory.fournisseurDTOToFournisseur(fournisseurDTO);
        fournisseur = fournisseurRepository.save(fournisseur);
        FournisseurDTO resultDTO = FournisseurFactory.fournisseurToFournisseurDTO(fournisseur);
        return resultDTO;
    }

    /**
     * Update a fournisseurDTO.
     *
     * @param fournisseurDTO
     * @return the updated entity
     */
    public FournisseurDTO update(FournisseurDTO fournisseurDTO) {
        log.debug("request to udpdate fournisseur{} ", fournisseurDTO);
        Fournisseur fournisseur = fournisseurRepository.findById(fournisseurDTO.getId()).orElseThrow(() -> new IllegalArgumentException("fournisseur Not Found"));
        Fournisseur updateFournisseur = FournisseurFactory.fournisseurDTOToFournisseur(fournisseurDTO);
        updateFournisseur.setId(fournisseur.getId());
        updateFournisseur = fournisseurRepository.save(updateFournisseur);
        return FournisseurFactory.fournisseurToFournisseurDTO(updateFournisseur);


    }

    /**
     * Get one fournisseurDTO by id.
     *
     * @param id the id of the entity
     * @return the entity DTO
     */
    @Transactional(
            readOnly = true
    )
    public FournisseurDTO findById(Integer id) {
        log.debug("Request to get Fournisseur: {}", id);
        Fournisseur fournisseur = fournisseurRepository.findById(id).orElse(null);
        FournisseurDTO dto = FournisseurFactory.fournisseurToFournisseurDTO(fournisseur);
        return dto;
    }

    /**
     * Get one fournisseur by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(
            readOnly = true
    )
    public Fournisseur findFournisseur(Integer id) {
        log.debug("Request to get Fournisseur: {}", id);

        return fournisseurRepository.findById(id).orElse(null);
    }

    /**
     * Get all the fournisseurs.
     *
     * @return the the list of entities
     */
    @Transactional(
            readOnly = true
    )
    public Collection<FournisseurDTO> findAll() {
        log.debug("Request to get All Fournisseurs");
        Collection<Fournisseur> result = fournisseurRepository.findAll();
        return FournisseurFactory.fournisseurToFournisseurDTOs(result);
    }

    /**
     * Delete fournisseur by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete Fournisseur: {}", id);
        fournisseurRepository.deleteById(id);
    }
}

