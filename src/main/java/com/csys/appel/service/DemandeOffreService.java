package com.csys.appel.service;

import com.csys.appel.domain.DemandeOffre;
import com.csys.appel.dto.DemandeOffreDTO;
import com.csys.appel.factory.DemandeOffreFactory;
import com.csys.appel.repository.DemandeOffreRepository;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing DemandeOffre.
 */
@Service
@Transactional
public class DemandeOffreService {
    private final Logger log = LoggerFactory.getLogger(DemandeOffreService.class);

    private final DemandeOffreRepository demandeoffreRepository;

    public DemandeOffreService(DemandeOffreRepository demandeoffreRepository) {
        this.demandeoffreRepository = demandeoffreRepository;
    }

    /**
     * Save a demandeoffreDTO.
     *
     * @param demandeoffreDTO
     * @return the persisted entity
     */
    public DemandeOffreDTO save(DemandeOffreDTO demandeoffreDTO) {
        log.debug("Request to save DemandeOffre: {}", demandeoffreDTO);
        DemandeOffre demandeoffre = DemandeOffreFactory.demandeoffreDTOToDemandeOffre(demandeoffreDTO);
        demandeoffre = demandeoffreRepository.save(demandeoffre);
        DemandeOffreDTO resultDTO = DemandeOffreFactory.demandeoffreToDemandeOffreDTO(demandeoffre);
        return resultDTO;
    }

    /**
     * Update a demandeoffreDTO.
     *
     * @param demandeoffreDTO
     * @return the updated entity
     */
    public DemandeOffreDTO update(DemandeOffreDTO demandeoffreDTO) {
        log.debug("Request to update DemandeOffre: {}", demandeoffreDTO);
        DemandeOffre demandeoffre = demandeoffreRepository.findById(demandeoffreDTO.getId())
                .orElseThrow(() -> new IllegalArgumentException("DemandeOffre not found"));
        DemandeOffre updatedDemandeOffre = DemandeOffreFactory.demandeoffreDTOToDemandeOffre(demandeoffreDTO);
        updatedDemandeOffre.setId(demandeoffre.getId()); // Ensure the ID remains the same
        updatedDemandeOffre = demandeoffreRepository.save(updatedDemandeOffre);
        return DemandeOffreFactory.demandeoffreToDemandeOffreDTO(updatedDemandeOffre);
    }

    /**
     * Get one demandeoffreDTO by id.
     *
     * @param id the id of the entity
     * @return the entity DTO
     */
    @Transactional(readOnly = true)
    public DemandeOffreDTO findOne(Integer id) {
        log.debug("Request to get DemandeOffre: {}", id);
        DemandeOffre demandeoffre = demandeoffreRepository.findById(id)
                .orElse(null);
        return DemandeOffreFactory.demandeoffreToDemandeOffreDTO(demandeoffre);
    }

    /**
     * Get one demandeoffre by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public DemandeOffre findDemandeOffre(Integer id) {
        log.debug("Request to get DemandeOffre: {}", id);
        return demandeoffreRepository.findById(id)
                .orElse(null);
    }

    /**
     * Get all the demandeoffres.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Collection<DemandeOffreDTO> findAll() {
        log.debug("Request to get All DemandeOffres");
        Collection<DemandeOffre> result = demandeoffreRepository.findAll();
        return DemandeOffreFactory.demandeoffreToDemandeOffreDTOs(result);
    }

    /**
     * Delete demandeoffre by id.
     *
     * @param id the id of the entity
     */
    public void delete(Integer id) {
        log.debug("Request to delete DemandeOffre: {}", id);
        demandeoffreRepository.deleteById(id);
    }
}
