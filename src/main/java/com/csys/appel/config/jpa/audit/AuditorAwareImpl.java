package com.csys.appel.config.jpa.audit;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this appel file, choose Tools | appels
 * and open the appel in the editor.
 */

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

/**
 *
 * @author Admin
 */
public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of (SecurityContextHolder.getContext().getAuthentication().getName());
    }

}
