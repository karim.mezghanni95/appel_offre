/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this appel file, choose Tools | appels
 * and open the appel in the editor.
 */
package com.csys.appel.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 *
 * @author Administrateur
 */
@Configuration
public class MessageConfigDev {

    @Bean
    @Profile("dev")
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource source = new ReloadableResourceBundleMessageSource();
        source.setBasename("classpath:/i18n/messages");
        source.setUseCodeAsDefaultMessage(true);
        source.setDefaultEncoding("UTF-8");
        return source;
    }

}
