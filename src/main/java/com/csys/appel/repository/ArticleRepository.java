package com.csys.appel.repository;

import com.csys.appel.domain.Article;
import java.lang.Boolean;
import java.lang.Integer;
import java.util.Collection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Article entity.
 */
@Repository
public interface ArticleRepository extends JpaRepository<Article, Integer> {
  Collection<Article> findByActif(Boolean actif);
}

