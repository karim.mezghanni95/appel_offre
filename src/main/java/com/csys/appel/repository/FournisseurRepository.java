package com.csys.appel.repository;

import com.csys.appel.domain.Fournisseur;
import java.lang.Integer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Fournisseur entity.
 */
@Repository
public interface FournisseurRepository extends JpaRepository<Fournisseur, Integer> {
}

