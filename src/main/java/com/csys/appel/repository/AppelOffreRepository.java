package com.csys.appel.repository;

import com.csys.appel.domain.AppelOffre;
import java.lang.Integer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the AppelOffre entity.
 */
@Repository
public interface AppelOffreRepository extends JpaRepository<AppelOffre, Integer> {
}

