package com.csys.appel.repository;

import com.csys.appel.domain.Categorie;
import java.lang.Boolean;
import java.lang.Integer;
import java.util.Collection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Categorie entity.
 */
@Repository
public interface CategorieRepository extends JpaRepository<Categorie, Integer> {
  Collection<Categorie> findByActif(Boolean actif);
}

