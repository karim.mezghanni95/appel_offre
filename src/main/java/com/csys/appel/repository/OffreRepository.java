package com.csys.appel.repository;

import com.csys.appel.domain.Offre;
import java.lang.Integer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Offre entity.
 */
@Repository
public interface OffreRepository extends JpaRepository<Offre, Integer> {
}

