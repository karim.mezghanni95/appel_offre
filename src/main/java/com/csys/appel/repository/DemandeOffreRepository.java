package com.csys.appel.repository;

import com.csys.appel.domain.DemandeOffre;
import java.lang.Integer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the DemandeOffre entity.
 */
@Repository
public interface DemandeOffreRepository extends JpaRepository<DemandeOffre, Integer> {
}

