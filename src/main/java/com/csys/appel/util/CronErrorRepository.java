/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this appel file, choose Tools | appels
 * and open the appel in the editor.
 */
package com.csys.appel.util;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author bassatine
 */
public interface CronErrorRepository extends JpaRepository<CronError, Integer> {
        
}
