/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this appel file, choose Tools | appels
 * and open the appel in the editor.
 */
package com.csys.appel.util;

import com.querydsl.core.types.dsl.BooleanExpression;

  @FunctionalInterface
    public interface LazyBooleanExpression<T>
    {
        BooleanExpression get();
    }